package demo.jmoreno.PostCrud.controller;

import demo.jmoreno.PostCrud.model.Post;
import demo.jmoreno.PostCrud.model.PostForm;
import demo.jmoreno.PostCrud.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class PostController {

    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping(value = "/")
    public ModelAndView showPosts(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "4") int size,
            @ModelAttribute("deleteMessage") String flashAttribute
    ) {
        Page<Post> posts = postService.getPage(page, size);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("pagination");
        mv.getModel().put("posts", posts);
        mv.getModel().put("errorMessage", flashAttribute);
        return mv;
    }

    @GetMapping(value = "/create")
    public ModelAndView createPost(@ModelAttribute("errorMessage") String flashAttribute) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("form");
        mv.getModel().put("postForm", new PostForm());
        mv.getModel().put("errorMessage", flashAttribute);
        return mv;
    }

    @GetMapping(value = "/post/{id}")
    public ModelAndView view(@PathVariable("id") long id,
                             @ModelAttribute("successMessage") String flashAttribute) {
        Optional<Post> optionalPost = postService.findById(id);
        return optionalPost
                .map(post -> {
                    ModelAndView mv = new ModelAndView();
                    mv.setViewName("view");
                    mv.getModel().put("post", post);
                    mv.getModel().put("successMessage", flashAttribute);
                    return mv;
                })
                .orElseGet(() -> new ModelAndView("redirect:/"));

    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView editPost(@PathVariable("id") long id) {
        Optional<Post> optionalPost = postService.findById(id);
        return optionalPost
                .map(post -> new ModelAndView("form", "postForm", PostForm.map(post)))
                .orElseGet(() -> new ModelAndView("redirect:/"));

    }

    @GetMapping(value = "/delete/{id}")
    public RedirectView deletePost(@PathVariable("id") long id, RedirectAttributes attributes) {
        Optional<Post> optionalPost = postService.findById(id);
        return optionalPost
                .map(post -> {
                    postService.delete(post);
                    attributes.addFlashAttribute("deleteMessage", "Delete success");
                    return new RedirectView("/");
                })
                .orElseGet(() -> {
                    attributes.addFlashAttribute("deleteMessage", "Delete failed");
                    return new RedirectView("/");
                });
    }

    @PostMapping(value = "/save")
    public RedirectView savePost(
            @Valid @ModelAttribute("postForm") PostForm postForm,
            BindingResult result,
            RedirectAttributes attributes) {

        if (result.hasErrors()) {
            attributes.addFlashAttribute("errorMessage", "Save failed");
            return new RedirectView("/create");
        }

        Post post = postService.savePost(postForm);
        attributes.addFlashAttribute("successMessage", "Save success");
        return new RedirectView("/post/" + post.getId());
    }
}
