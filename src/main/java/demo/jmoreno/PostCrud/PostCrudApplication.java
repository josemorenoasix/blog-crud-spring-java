package demo.jmoreno.PostCrud;

import demo.jmoreno.PostCrud.storage.StorageProperties;
import demo.jmoreno.PostCrud.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class PostCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostCrudApplication.class, args);
    }

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.init();
        };
    }

}

