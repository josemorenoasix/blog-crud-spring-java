package demo.jmoreno.PostCrud.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "gnr_post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "post_title")
    private String title;

    @NotBlank
    @Size(max = 100)
    @Column(name = "post_slug")
    private String slug;

    @NotBlank
    @Size(max = 255)
    @Column(name = "post_abstract")
    private String summary;

    @NotBlank
    @Column(name = "post_body")
    @Lob
    private String body;

    @NotNull
    @Column(name = "post_date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @NotNull
    @Column(name = "post_visible",
            columnDefinition = "ENUM('VISIBLE','HIDDEN')")
    @Enumerated(EnumType.STRING)
    private PostStatus isVisible;

    @NotBlank
    @Size(max = 200)
    @Column(name = "post_image")
    private String imagePath;

    public Post() {
    }

    public Post(
            @NotBlank @Size(max = 100) String title,
            @NotBlank @Size(max = 100) String slug,
            @NotBlank @Size(max = 255) String summary,
            @NotBlank String body,
            @NotNull Date date,
            @NotNull PostStatus isVisible,
            @NotBlank @Size(max = 200) String imagePath) {
        this.title = title;
        this.slug = slug;
        this.summary = summary;
        this.body = body;
        this.date = date;
        this.isVisible = isVisible;
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public PostStatus getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(PostStatus isVisible) {
        this.isVisible = isVisible;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}


