package demo.jmoreno.PostCrud.model;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;

public class PostForm {

    private Long id;

    @NotBlank
    @Size(max = 100)
    private String title;

    @NotBlank
    @Size(max = 255)
    private String summary;

    @NotBlank
    private String body;

    @NotNull
    private boolean isVisible;

    private MultipartFile image;

    private String imagePath = "";


    @AssertFalse(message = "se requiere una imagen")
    private boolean isEmpty() {
        return image.isEmpty() && imagePath.isEmpty();
    }

    public PostForm() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        this.isVisible = visible;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public static PostForm map(Post post) {
        PostForm pf = new PostForm();
        pf.setId(post.getId());
        pf.setTitle(post.getTitle());
        pf.setSummary(post.getSummary());
        pf.setBody(post.getBody());
        pf.setVisible(post.getIsVisible() == PostStatus.VISIBLE);
        pf.setImagePath(post.getImagePath());
        return pf;
    }
}
