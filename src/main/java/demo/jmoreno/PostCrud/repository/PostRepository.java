package demo.jmoreno.PostCrud.repository;

import demo.jmoreno.PostCrud.model.Post;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PostRepository extends PagingAndSortingRepository<Post, Long> {
}
