package demo.jmoreno.PostCrud.service;

import com.github.slugify.Slugify;
import demo.jmoreno.PostCrud.model.Post;
import demo.jmoreno.PostCrud.model.PostForm;
import demo.jmoreno.PostCrud.model.PostStatus;
import demo.jmoreno.PostCrud.repository.PostRepository;
import demo.jmoreno.PostCrud.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

@Service
public class PostService {

    private PostRepository postRepository;
    private StorageService storageService;

    @Autowired
    public PostService(PostRepository postRepository,
                       StorageService storageService) {
        this.postRepository = postRepository;
        this.storageService = storageService;
    }

    public Page<Post> getPage(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "date");
        return this.postRepository.findAll(pageable);
    }

    public Post savePost(PostForm postForm) {

        String filename;

        if (postForm.getImage().isEmpty()) {
            filename = postForm.getImagePath();
        } else {
            if (!postForm.getImagePath().isEmpty()) {
                this.storageService.delete(postForm.getImagePath());
            }
            filename = this.storageService.store(postForm.getImage());
        }

        Post post = new Post(
                postForm.getTitle(),
                new Slugify().slugify(postForm.getTitle()),
                postForm.getSummary(),
                postForm.getBody(),
                Date.from(Instant.now()),
                (postForm.isVisible()) ? PostStatus.VISIBLE : PostStatus.HIDDEN,
                filename
        );

        if (postForm.getId() != null) {
            post.setId(postForm.getId());
        }

        return this.postRepository.save(post);
    }

    public Optional<Post> findById(long id) {
        return this.postRepository.findById(id);
    }

    public void delete(Post post) {
        this.storageService.delete(post.getImagePath());
        this.postRepository.delete(post);
    }
}
