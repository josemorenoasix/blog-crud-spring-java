<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"/>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form:form method="POST" action="/save" enctype="multipart/form-data"
                               modelAttribute="postForm">

                        <div class="row justify-content-center">
                            <form:hidden path="id"/>
                            <div class="form-group col-md-10 mb-3">
                                <form:input path="title" size="100" cssClass="form-control" placeholder="Title..." />
                            </div>
                            <div class="form-group col-md-2 mb-3">
                                <form:checkbox path="visible" cssClass="form-control" id="toggle-visible"
                                               data-toggle="toggle" data-on="Active" data-off="Disabled"
                                               data-onstyle="success" data-offstyle="danger"/>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="form-group col-md-12 mb-3">
                                <form:textarea path="summary" cssClass="form-control" placeholder="Resumen..." />
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="form-group col-md-10 mb-3 border rounded">
                                <div>
                                    <img id="preview-image" class="upload-preview img-thumbnail img-fluid mx-auto d-block" src="/uploads/files/${postForm.imagePath}" alt="your image" />
                                    <form:hidden path="imagePath"/>
                                </div>
                            </div>
                            <div class="form-group col-md-10 mb-3">
                                <label class="custom-file-label" for="input-image" id="label-upload">Choose file</label>
                                <input id="input-image" name="image" type="file" class="custom-file-input"
                                       accept="image/png, image/jpeg, image/gif" onchange="readURL(this);"/>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-12 mb-3">
                                <form:textarea path="body" cssClass="form-control" title="editor" id="summernote" tabindex=""/>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-8 mb-3">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Save Post</button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
<c:choose>
    <c:when test="${errorMessage.length() != 0 && errorMessage.length() != null}">
        <div class="fixed-bottom alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error!</strong> ${errorMessage.length()}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </c:when>
</c:choose>
<jsp:include page="footer.jsp"/>