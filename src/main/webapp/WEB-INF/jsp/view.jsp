<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"/>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img class="" src="/uploads/files/${post.imagePath}" alt="Image cap" style="width: 100%">
                    <div class="">
                        <h5 class="">${post.title}</h5>
                        <p class="">${post.summary}</p>
                    </div>
                    <div class="">
                        ${post.body}
                    </div>
                    <div class="">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a role="button" href="<c:url value="/edit/${post.id}"/>" class="btn btn-sm btn-outline-secondary">Edit</a>
                                <button id="delete-button" data-toggle="modal" data-target="#deleteAlert" class="btn-danger btn-sm" value="${post.id}">Delete</button>
                            </div>
                            <small class="text-muted">${post.date.toLocaleString()}</small>
                            ${successMessage.toString()}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<c:choose>
    <c:when test="${successMessage.length() != 0}">
        <div class="fixed-bottom alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Message!</strong> ${successMessage.toString()}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </c:when>
</c:choose>
<jsp:include page="footer.jsp"/>