
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                    <c:forEach items="${posts.iterator()}" var="post">
                        <div class="col-md-6 d-flex align-items-stretch">
                            <div class="card mb-6 h-100 w-100 box-shadow">
                                <a class="page-link" href="<c:url value="/post/${post.id}"/> ">
                                    <img class="card-img-top img-thumbnail" src="/uploads/files/${post.imagePath}" alt="Card image cap">
                                </a>

                                <div class="card-body">
                                    <a class="page-link" href="<c:url value="/post/${post.id}"/> ">
                                        <h5 class="card-title">${post.title}</h5>
                                    </a>
                                    <a href="?action=read&id=$id">

                                    </a>
                                    <p class="card-text">${post.summary}</p>
                                </div>

                                <div class="card-footer text-muted">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a role="button" href="<c:url value="/post/${post.id}"/>" class="btn btn-sm btn-outline-secondary">Read</a>
                                            <a role="button" href="<c:url value="/edit/${post.id}"/>" class="btn btn-sm btn-outline-secondary">Edit</a>
                                        </div>
                                        <small class="text-muted">${post.date}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                <div class="col-md-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">

                            <c:choose>
                            <c:when test="${posts.hasPrevious()}">
                                <li class="page-item ">
                                    <a class="page-link" href="<c:url value="/?page=${posts.number - 1}"/> " aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                            </c:otherwise>
                            </c:choose>
                                <c:forEach begin="1" end="${posts.totalPages}" var="i">
                                <li class="page-item"><a class="page-link" href="<c:url value="/?page=${i-1}"/>">${i}</a></li>
                                </c:forEach>
                                <c:choose>
                                <c:when test="${posts.hasNext()}">
                                <li class="page-item" >
                                    <a class="page-link" href="<c:url value="/?page=${posts.number + 1}"/>" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                                </c:when>
                                <c:otherwise>
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                                </c:otherwise>
                                </c:choose>
            </div>
        </div>
    </div>
        <c:choose>
        <c:when test="${deleteMessage.length() != 0}">
        <div class="fixed-bottom alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Message!</strong> ${deleteMessage.toString()}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        </c:when>
        </c:choose>
<jsp:include page="footer.jsp"/>
