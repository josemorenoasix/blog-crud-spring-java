<!-- Modal -->
<div class="modal fade" id="deleteAlert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="delete-confirm-button" type="button" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>
<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>
</body>
</html>


<script>
    $('#summernote').summernote({
        placeholder: '...',
        minHeight: 200,             // set minimum height of editor
        maxHeight: 400,             // set maximum height of editor
    });

    $(function () {
        $('#delete-confirm-button').click(function () {
            window.location = '/delete/' + $('#delete-button').val();
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-image')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            $('#label-upload')
                .text(input.files[0].name);

        }
    }
</script>